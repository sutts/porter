class DocumentListController extends BaseController

  constructor: (@scope, @http, @route, @location) ->
    @scope.projectId = @route.current.params.projectId
    @scope.newDocument = { month: new Date }
    @scope.create = =>
      period = moment(@scope.newDocument.month).format 'YYYY/MM'
      @location.path "#{@location.path()}/new/#{period}"

    @http.get("/projects/#{@scope.projectId}/documents").then(@init, @failure)

  init: (response) =>
    console.log "success", response.data
    @scope.documents = response.data


window?.DocumentListController = DocumentListController