defaultStructure = '''
Introduction
Fruits
  Apple
  Banana
Vegetables
  Carrot
  Turnip
Summary
'''

class DocumentNewController extends BaseController

  constructor: (@scope, @http, @route, @location) ->
    @parser = new DefinitionParser

    @scope.project =
      structure: defaultStructure

    @scope.aceLoaded = (editor) ->
      editor.$blockScrolling = Infinity

    @scope.aceChanged = (evt) =>
      lines = @scope.project.structure.split '\n'
      tree = @parser.parse(lines).enumerate()
      @scope.sections = tree.toArray().splice(1)
      section.padding = (section.level() - 1) * 20 for section in @scope.sections

    @scope.create = =>
      projectId = @route.current.params.projectId
      payload =
        projectId: projectId
        year: @route.current.params.year
        month: @route.current.params.month
        structure: @scope.project.structure
        sections: (section.toResource() for section in @scope.sections)
      console.log 'payload', payload
      @http.post("/projects/#{projectId}/documents", payload).then(@documentCreated, @failure)

  documentCreated: (response) =>
    console.log 'documentCreated', response
    @location.path "/#{@route.current.params.projectId}/docs/#{response.data.id}"


window?.DocumentNewController = DocumentNewController


