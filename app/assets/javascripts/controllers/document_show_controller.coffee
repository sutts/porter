class DocumentShowController extends BaseController

  constructor: (@scope, @http, @route) ->
    @parser = new DefinitionParser
    @scope.projectId = @route.current.params.projectId
    @scope.docId = @route.current.params.docId
    @http.get("/projects/#{@scope.projectId}/documents/#{@scope.docId}").then(@success, @failure)

  success: (response) =>
    console.log "DocumentShow:", response.data
    structure = response.data.structure.body
    lines = structure.split '\n'
    tree = @parser.parse(lines).enumerate()
    @scope.sections = tree.toArray().splice(1)
    section.padding = (section.level() - 1) * 20 for section in @scope.sections
    for page in response.data.pages
      lastModified = moment page.modifiedDate
      created = moment page.createdDate
      page.virgin = lastModified.diff(created) < 5000
    for section in @scope.sections
      for page in response.data.pages
        section.page = page if page.title is section.name

    console.log @scope.sections

window?.DocumentShowController = DocumentShowController