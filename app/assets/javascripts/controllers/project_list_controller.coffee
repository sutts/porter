class ProjectListController extends BaseController

  constructor: (@scope, @http) ->
    @scope.newProject = {}
    @scope.create = =>
      payload =
        name: @scope.newProject.name
      @http.post('/projects', payload).then(@projectCreated, @failure)
      @scope.newProject = {}
    @http.get('/projects').then(@init, @failure)

  init: (response) =>
    console.log "init project list with", response.data
    @scope.projects = response.data

  projectCreated: (response) =>
    console.log "project created with ", response.data
    @scope.projects.push { id: response.data.id, title: response.data.title }


window?.ProjectListController = ProjectListController