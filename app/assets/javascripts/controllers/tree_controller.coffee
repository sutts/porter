class TreeController extends BaseController

  constructor: (@scope, @http) ->
    root = new Node 'My Document'
    child1 = root.addChild 'Section 1'
    child2 = root.addChild 'Section 2'
    grandchild1 = child1.addChild 'Section 1.1'
    grandchild2 = child1.addChild 'Section 1.2'
    grandchild3 = child2.addChild 'Section 2.1'
    grandchild4 = child2.addChild 'Section 2.2'

    @scope.sections = root.toArray()
#    [
#      { title: 'Section 1',     level: 0 }
#      { title: 'Section 1.1',   level: 1 }
#      { title: 'Section 1.2',   level: 1 }
#      { title: 'Section 1.2.1', level: 2 }
#      { title: 'Section 1.2.2', level: 2 }
#      { title: 'Section 2',     level: 0 }
#      { title: 'Section 3',     level: 0 } ]

    @scope.padding = (section) -> section.level() * 20
    @scope.newSection = {}
    @scope.add = =>
      @scope.sections.push @scope.newSection
      @scope.newSection = {}
    @scope.beginUpdate = (section) =>
      s.editing = false for s in @scope.sections
      section.editing = true
    @scope.cancelUpdate = (section) =>
      section.editing = false
    @scope.update = (section) =>
      section.editing = false
    @scope.indent = (section) =>
      console.log section
      section.level++
    @scope.outdent = (section) =>
      console.log section
      section.level-- if section.level >= 0

window.TreeController = TreeController if window
