class DefinitionParser

  parseLine: (line) ->
    indentOf = (line) ->
      for x in [0...line.length]
        return x unless line.charAt(x) is ' '
      -1
    indent = indentOf line
    if indent >= 0
      tokens = line.trim().split '//'
      lineInfo = { indent: indent, title: tokens[0].trim() }
      if tokens.length > 1
        for pair in tokens[1].trim().split(' ')
          pairTokens = pair.split('=')
          if pairTokens.length is 2 and pairTokens[0].trim().length > 0 and pairTokens[1].trim().length
            lineInfo[pairTokens[0].trim()] = pairTokens[1].trim()
      lineInfo
    else
      undefined

  parse: (lines) ->
    findParent = (candidate, indent) -> if indent > candidate.indent then candidate else findParent(candidate.parent, indent)
    root = new Node 'root'
    root.indent = -1
    parent = root
    for line in lines
      parsedLine = @parseLine line
      if parsedLine?
        parent = findParent parent, parsedLine.indent
        child = parent.addChild parsedLine.title
        child.indent = parsedLine.indent
        child.key = parsedLine.key
        child.owner = parsedLine.owner
        parent = child
    root

window?.DefinitionParser = DefinitionParser

