class Node

  constructor: (@name) ->
    @children = []

  addChild: (name) ->
    child = new Node name
    child.parent = @
    @children.push child
    child

  level: ->
    if @parent? then @parent.level() + 1 else 0

  implicitOwner: ->
    @owner ? @parent?.implicitOwner()

  toArray: ->
    list = [@]
    for child in @children
      for descendant in child.toArray()
        list.push descendant
    list

  toString: ->
    stringifieds = (child.toString() for child in @children)
    colon = if @children.length > 0 then ': ' else ''
    "[#{@name}#{colon}#{stringifieds.join ', '}]"

  toResource: ->
    { name: @name, key: @key }

  enumerate: ->
    prefix = if @number then "#{@number}." else ""
    for child, i in @children
      child.number = "#{prefix}#{i+1}"
      child.enumerate()
    @

window?.Node = Node


