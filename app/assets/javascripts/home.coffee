app = angular.module 'porter', ['ngRoute', 'templates', 'ui.ace']
  
app.config [ '$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->
  $routeProvider.when '/',                                      { templateUrl: 'project_list.html',   controller: 'ProjectListController' }
  $routeProvider.when '/:projectId/docs',                       { templateUrl: 'document_list.html',  controller: 'DocumentListController' }
  $routeProvider.when '/:projectId/docs/:docId',                { templateUrl: 'document_show.html',  controller: 'DocumentShowController' }
  $routeProvider.when '/:projectId/docs/:docId/:year/:month',   { templateUrl: 'document_new.html',   controller: 'DocumentNewController' }
]

app.controller 'ProjectListController',   ['$scope', '$http', ProjectListController]
app.controller 'DocumentListController',  ['$scope', '$http', '$route', '$location', DocumentListController]
app.controller 'DocumentShowController',  ['$scope', '$http', '$route', '$location', DocumentShowController]
app.controller 'DocumentNewController',   ['$scope', '$http', '$route', '$location', DocumentNewController]

app.directive 'page', [ '$http', '$sce', ($http, $sce) ->
  restrict: 'E'
  replace: 'true'
  templateUrl: 'document-page.html'
  scope:
    section: '='
  link: (scope, elem, attrs) ->
    scope.virgin = scope.section.page.virgin
    success = (response) =>
      # okay, this is naive
      matches = /<body(.*)<\/body>/.exec response.data
      content = if matches? then matches[0].replace('<body', '<div') else "Whoops, failed to extract body content"
      scope.content = $sce.trustAsHtml content
      console.log "content", content
    failure = (err) =>
      console.log "download failed", err
    downloadUrl = scope.section.page.exportLinks['text/html']
    $http.get(downloadUrl).then(success, failure) unless scope.virgin
]

app.directive 'relDate', ->
  restrict: 'AE'
  replace: 'true'
  template: '<span>{{relDate}}</span>'
  scope:
    date: '='
  link: (scope, elem, attrs) ->
    scope.relDate = moment(scope.date).fromNow()
