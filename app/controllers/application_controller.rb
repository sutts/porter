require 'google/apis/drive_v2'
require 'google/api_client/client_secrets'

class ApplicationController < ActionController::Base

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception

  def signet
    client = Signet::OAuth2::Client.new(
        :authorization_uri => 'https://accounts.google.com/o/oauth2/auth',
        :token_credential_uri =>  'https://www.googleapis.com/oauth2/v3/token',
        :client_id => '925026395109-v17d6h5cpotn5d0jl67tfcjntnv03n2v.apps.googleusercontent.com',
        :client_secret => 'PIb1z1aL19HeNHVkUyjPqg91',
        :scope => 'email profile https://www.googleapis.com/auth/drive',
        :redirect_uri => 'http://localhost:3000/oauth2callback',
        :access_token => session[:access_token]
    )
  end


end
