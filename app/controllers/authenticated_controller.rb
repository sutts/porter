class AuthenticatedController < ApplicationController

  include DriveHelper

  FOLDER_MIME_TYPE = 'application/vnd.google-apps.folder'

  before_action :init_drive

  # def create_folder(parent_id, title)
  #   metadata = Google::Apis::DriveV2::File.new title: title
  #   metadata = @drive.insert_file metadata, content_type: FOLDER_MIME_TYPE, parent: {:id => parent_id}
  #   # @drive.insert_child parent_id, metadata
  #   metadata
  # end

  private

  def init_drive
    if session[:access_token]
      # todo: check whether the access token has expired in which case do a redirect
      @drive = Google::Apis::DriveV2::DriveService.new
      @drive.authorization = signet
    else
      redirect_to '/oauth2callback'
    end
  end

end