require 'tempfile'

class DocumentsController < AuthenticatedController

  def index
    render json: documents(project_id)
  end

  def create

    year = params.fetch :year
    month = params.fetch :month
    title = "#{year}/#{month}"

    folder = create_document_folder project_id, title

    # using drive for file uploads, drivey for everything else
    drive = init_drive

    with_temp_file 'structure', params.fetch(:structure) do |tmp_file|
      metadata = Google::Apis::DriveV2::File.new title: '_structure'
      metadata = drive.insert_file metadata, upload_source: tmp_file.path, convert: true
      drive.insert_child folder.fetch(:id), metadata
    end

    with_temp_file 'template', '<!DOCTYPE html><html></html>' do |tmp_file|
      params.fetch(:sections).each do |section|
        metadata = Google::Apis::DriveV2::File.new title: section.fetch(:name)
        metadata = drive.insert_file metadata, upload_source: tmp_file.path, convert: true
        drive.insert_child folder.fetch(:id), metadata
      end
    end

    render json: folder, status: 201
  end

  def show
    response = {
        :structure =>  document_structure(project_id, document_id),
        :pages => document_pages(project_id, document_id)
    }
    render json: response
  end

  private

  def with_temp_file(name, content)
    file = Tempfile.new name
    file.write content
    file.close
    yield file if block_given?
    file.unlink
  end

  def project_id
    params.fetch :project_id
  end

  def document_id
    params.fetch :id
  end

  def init_drive
    if session[:access_token]
      # todo: check whether the access token has expired in which case do a redirect
      drive = Google::Apis::DriveV2::DriveService.new
      drive.authorization = signet
      drive
    else
      redirect_to '/oauth2callback'
    end
  end


end