class HomeController < ApplicationController

  def index
    redirect_to '/welcome' unless session[:access_token]
  end

  def welcome
  end

end