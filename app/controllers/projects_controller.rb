class ProjectsController < AuthenticatedController

  def index
    render json: projects
  end

  def create
    name = params.fetch :name
    project_folder = create_project_folder name
    render json: project_folder, status: 201
  end

end
