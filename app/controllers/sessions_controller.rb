require 'google/apis/drive_v2'
require 'google/api_client/client_secrets'

class SessionsController < ApplicationController

  def callback
    if params[:code]
      client = signet
      client.code = params[:code]
      response = client.fetch_access_token!
      session[:access_token] = response['access_token']
      puts "Access token: ", session[:access_token]
      # todo: don't throw away the refresh token
      redirect_to '/#/'
    else
      redirect_to signet.authorization_uri.to_s
    end
  end

end