module DriveHelper

  FOLDER_MIME_TYPE      = 'application/vnd.google-apps.folder'
  ROOT_FOLDER           = '0B7Ax6CsZBdnbYlF3Z1BqQ1R5dkU'
  TEMPLATE_FILE         = '1lGhfxAOpAisXXQrRiTqnh_-FtaIlRJUIJHmKSJrDPXw'
  STRUCTURE_FILE_TITLE  = '_structure'

  def projects
    Rails.cache.fetch("projects") do
      get_child_folders(ROOT_FOLDER).fetch(:items).map do |item|
        { id: item.fetch('id'), title: item.fetch('title') }
      end
    end
  end

  def documents(project_folder_id)
    Rails.cache.fetch("project.#{project_folder_id}.documents") do
      get_child_folders(project_folder_id).fetch(:items).map do |item|
        { id: item.fetch('id'), title: item.fetch('title') }
      end
    end
  end

  def document_structure(project_folder_id, document_folder_id)
    # running into encryption errors on any attempt to cache this
    q = URI.encode "title=\"#{STRUCTURE_FILE_TITLE}\" and \"#{document_folder_id}\" in parents"
    document = get "/drive/v2/files?q=#{q}"
    url = document[:items][0]['downloadUrl']
    client = Hurley::Client.new
    client.header[:authorization] = "Bearer #{session[:access_token]}"
    response = client.get url
    response
  end

  def document_pages(project_folder_id, document_folder_id)
    Rails.cache.fetch("projects#{project_folder_id}.#{document_folder_id}.pages") do
      get_child_files(document_folder_id).fetch :items
    end
  end

  def create_project_folder(name)
    Rails.cache.delete_matched(/projects/)
    create_folder ROOT_FOLDER, name
  end

  def create_document_folder(project_folder_id, name)
    Rails.cache.delete_matched(/project.#{project_folder_id}/)
    create_folder project_folder_id, name
  end

  def create_page(project_folder_id, document_folder_id, name)
    Rails.cache.delete_matched(/project.#{project_folder_id}.#{document_folder_id}/)
    payload = { :title => name, :parents => [ { :id => document_folder_id } ] }
    post "/drive/v2/files/#{TEMPLATE_FILE}/copy", payload
  end

  private

  def get_child_folders(folder_id)
    q = URI.encode "mimeType=\"#{FOLDER_MIME_TYPE}\" and \"#{folder_id}\" in parents"
    get "/drive/v2/files?q=#{q}"
  end

  def get_child_files(folder_id)
    q = URI.encode "\"#{folder_id}\" in parents"
    get "/drive/v2/files?q=#{q}"
  end

  def create_folder(parent_id, name)
    payload = { :title => name,
                :mimeType => FOLDER_MIME_TYPE,
                :parents => [ { :id => parent_id } ] }
    post '/drive/v2/files', payload
  end

  def get(path)
    response = client.get path
    if response.success?
      JSON.parse(response.body).symbolize_keys
    else
      puts "WHOOPS============================="
      puts path
      puts "<<path-----------------------------"
      puts response
      puts "<<response========================="
      raise 'whoops'
    end
  end

  def post(path, payload)
    response = client.post path, payload.to_json, 'application/json'
    if response.success?
      JSON.parse(response.body).symbolize_keys
    else
      puts "WHOOPS============================="
      puts path
      puts "<<path-----------------------------"
      puts payload
      puts "<<payload--------------------------"
      puts response
      puts "<<response========================="
      raise 'whoops'
    end
  end

  def client
    client = Hurley::Client.new 'https://www.googleapis.com'
    client.header[:authorization] = "Bearer #{session[:access_token]}"
    client.header[:content_type] = 'application/json'
    client
  end

end