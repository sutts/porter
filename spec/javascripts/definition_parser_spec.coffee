#= require domain/tree
#= require domain/definition_parser

describe "DefinitionParser", ->

  subject = new DefinitionParser

  describe "#parseLine", ->

    it "should recognise simple content", ->
      subject.parseLine('abc').should.deep.equal {title: 'abc', indent: 0}

    it "should handle left indentation", ->
      subject.parseLine('  abc').should.deep.equal {title: 'abc', indent: 2}

    it "should right trim title", ->
      subject.parseLine('  abc  ').should.deep.equal {title: 'abc', indent: 2}

    it "should gloss over content-less strings", ->
      (subject.parseLine('') is undefined).should.equal true
      (subject.parseLine('    ') is undefined).should.equal true

    it "should split on slash delimiter", ->
      subject.parseLine('abc // ').should.deep.equal {title: 'abc', indent: 0}

    it "should add key value pairs specified after the slash delimiter", ->
      expected = {title: 'abc', indent: 0, fruit: 'banana', vegetable: 'carrot'}
      subject.parseLine('abc // fruit=banana vegetable=carrot').should.deep.equal expected

    it "should gracefully ignore malformed key value pairs specified after the slash delimiter", ->
      expected = {title: 'abc', indent: 0, fruit: 'banana'}
      subject.parseLine('abc // vegetable: carrot =apple orange= fruit=banana ').should.deep.equal expected

  describe "#parse", ->

    it "should handle simple flat stuff", ->
      root = subject.parse ['Section 1', 'Section 2']
      root.toString().should.equal "[root: [Section 1], [Section 2]]"

    it "should handle nested sections", ->
      root = subject.parse ['Section 1', '  Section 1.1']
      root.toString().should.equal "[root: [Section 1: [Section 1.1]]]"
