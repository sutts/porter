#= require domain/tree

describe "Node", ->

  root = child1 = child2 = grandchild1 = grandchild2 = grandchild3 = grandchild4 = undefined

  beforeEach ->
    root = new Node 'root'
    child1 = root.addChild 'child 1'
    child2 = root.addChild 'child 2'
    grandchild1 = child1.addChild 'grandchild 1'
    grandchild2 = child1.addChild 'grandchild 2'
    grandchild3 = child2.addChild 'grandchild 3'
    grandchild4 = child2.addChild 'grandchild 4'

  describe "#level()", ->

    it "should reflect depth in tree", ->
      root.level().should.equal 0
      child1.level().should.equal 1
      grandchild1.level().should.equal 2

  describe "#enumerate()", ->

    it "should add numbers", ->
      root.enumerate()
      child1.number.should.equal "1"
      grandchild1.number.should.equal "1.1"

  describe "#toArray()", ->

    it "should return a flat list in order", ->
      grandchild1.toArray().should.deep.equal [grandchild1]
      child1.toArray().should.deep.equal [child1, grandchild1, grandchild2]
      root.toArray().should.deep.equal [root, child1, grandchild1, grandchild2, child2, grandchild3, grandchild4]

  describe "#toString()", ->
    it "should return a structured string useful for testing and debugging", ->
      grandchild1.toString().should.equal "[grandchild 1]"
      child1.toString().should.equal "[child 1: [grandchild 1], [grandchild 2]]"
      root.toString().should.equal "[root: [child 1: [grandchild 1], [grandchild 2]], [child 2: [grandchild 3], [grandchild 4]]]"

