doc = []
File.readlines('structure').each do |line|
  entry = { :title => line.strip, :sections => []}
  if line[0] == ' '
    doc.last[:sections] << entry
  else
    doc << entry
  end
end

require 'json'
puts JSON.pretty_generate(doc)